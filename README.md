# Python Lolesports-API

Python implementation of the unoffical Lolesports API of Riot Games. Documentation of the API can be found here: https://vickz84259.github.io/lolesports-api-docs/.

Also includes a small example.

## Installation

Download Python Lolesports-API

`git clone https://gitlab.com/leolo/lolesports-api-python.git`

Install dependency

`pip3 install requests`